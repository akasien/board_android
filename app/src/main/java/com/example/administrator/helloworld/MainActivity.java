package com.example.administrator.helloworld;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    Button button;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.btn);
        textView = (TextView)findViewById(R.id.txt_view);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Click!!", Toast.LENGTH_SHORT).show();
                setContentView(R.layout.activity_main);

                Intent intent = new Intent(MainActivity.this, PictureActivity.class);
                startActivity(intent);

            }
        });

    }

}

